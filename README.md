This code snippet can be added to a batch script to prompt for a password before continuing.

To start using batpass, there are two things that must be done to the batch script:
1. Replace all asterisks with the number of characters that the password will be.
2. Replace all ampersands with the desired password, unencrypted.